CREATE SCHEMA IF NOT EXISTS acct;

create table if not exists acct.clients(
  id        bigint auto_increment primary key,
  full_name VARCHAR(100)
);

create sequence if not exists acct.seq_account_number start 1000000000000000000;

create table if not exists acct.accounts(
  acct_num        bigint default next value for acct.seq_account_number,
  client_id       bigint references acct.clients(id),
  currency        varchar(10),
  current_balance numeric default 1000 -- Assume bank gives 1000 for all new accounts :))
);

create sequence if not exists acct.seq_transaction_number start 1000000000000000000;

create table if not exists  acct.money_transactions (
  tr_num                bigint default next value for acct.seq_transaction_number,
  acct_num              varchar(100) references acct.accounts(acct_num),
  counterpart_acct_num  varchar(100),
  oper_date             timestamp,
  amount                numeric,
  operation_type        varchar(10) check (operation_type
                                     in ('block', 'in', 'out', 'cancel')),
  primary key (tr_num, acct_num)
);