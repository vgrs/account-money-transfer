package net.sf.vgrs.moneyTransfer.utlis;

public class UserDefinedException extends Exception{

    private Errors error;

    public UserDefinedException(Errors error) {
        super(error.format());
        this.error = error;
    }

    public UserDefinedException(Errors error, Exception e) {
        super(error.format(), e);
        this.error = error;
    }

    public UserDefinedException(Errors error, String message) {
        super(error.format(message));
        this.error = error;
    }

    public UserDefinedException() {
        super();
    }

    public UserDefinedException(String message) {
        super(message);
    }

    public UserDefinedException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserDefinedException(Throwable cause) {
        super(cause);
    }

    protected UserDefinedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public Errors getError() {
        return error;
    }

    public enum Errors {
        ERROR_NOT_IMPLEMENTED("#500.00", "Requested operation not implemented yet"),
        ERROR_INTERNAL("#500.01", "Some problem in connection to database"),
        ERROR_TIMEOUT("#500.02", "Timeout in connection to database"),
        ERROR_DB_CONNECTION("#500.03", "Some problem in connection to database"),
        ERROR_DB_CALL("#500.04", "Some problem in calling database"),
        ERROR_BUSINESS_LEVEL("#400.02", "Business level error");

        private final String code;
        private final String description;

        Errors(String code, String description){
            this.code = code;
            this.description = description;
        }

        public String getCode() {
            return code;
        }

        public String getDescription() {
            return description;
        }

        public String format() {
            return String.format(format, code, description);
        }

        public String format(String message) {
            return String.format(formatWithMessage, code, description, message);
        }

        private static final String format = "(%s) - %s";
        private static final String formatWithMessage = "(%s) - %s : %s";
    }
}
