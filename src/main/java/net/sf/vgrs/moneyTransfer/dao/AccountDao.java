package net.sf.vgrs.moneyTransfer.dao;

import net.sf.vgrs.moneyTransfer.domain.Account;
import net.sf.vgrs.moneyTransfer.domain.MoneyTransactionInfo;
import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException;

import java.math.BigDecimal;
import java.util.List;

public interface AccountDao {

    Long addAccount(Account account) throws UserDefinedException;

    boolean updateAccountBalance(long accountId, BigDecimal amountToOperate) throws UserDefinedException;

    Account getAccountInfo(long accountNum) throws UserDefinedException;

    List<Account> getAccounts(long clientId);

    List<Account> getAccounts();

    MoneyTransactionInfo block(Long accountNumber,
                               Long counterPartAccountNumber,
                               BigDecimal amount) throws UserDefinedException;

    /**
     * The method to add money to account
     * @return
     */
    boolean credit(MoneyTransactionInfo info) throws UserDefinedException;

    /**
     * The method to remove money from account
     * @return
     */
    boolean debit(Long accountNum, Long transactionNumber) throws UserDefinedException;

}
