package net.sf.vgrs.moneyTransfer.dao.jdbc;

import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException;

import javax.sql.DataSource;

public interface DataSourceManager{

    DataSource get() throws UserDefinedException;

    void set() throws UserDefinedException;
}
