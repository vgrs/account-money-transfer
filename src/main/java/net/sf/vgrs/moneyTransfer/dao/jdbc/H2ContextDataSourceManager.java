package net.sf.vgrs.moneyTransfer.dao.jdbc;

import net.sf.vgrs.moneyTransfer.utlis.Constants.AppConfigKeys;
import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException;
import org.h2.jdbcx.JdbcDataSource;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.sql.DataSource;

@Singleton
public class H2ContextDataSourceManager implements DataSourceManager {
    public static final String DATASOURCE_PATH = "jdbc/datasource-path";
    private JdbcDataSource ds;

    @Inject
    public H2ContextDataSourceManager(
            @Named(AppConfigKeys.APP_DB_JDBC_H2_URL) String jdbcUrl,
            @Named(AppConfigKeys.APP_DB_JDBC_H2_USERNAME) String username,
            @Named(AppConfigKeys.APP_DB_JDBC_H2_PASSWORD) String password){
        ds = new JdbcDataSource();
        ds.setURL(jdbcUrl);
        ds.setUser(username);
        ds.setPassword(password);
    }

    @Override
    public DataSource get() throws UserDefinedException {
        return ds;
    }

    @Override
    public void set() throws UserDefinedException {

    }
}
