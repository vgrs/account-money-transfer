package net.sf.vgrs.moneyTransfer.dao;


import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException;

public abstract class ConnectionManager {

    public abstract void openConnection() throws UserDefinedException;

    public abstract void closeConnection() throws UserDefinedException;

    public abstract void commit() throws UserDefinedException;

    public abstract void rollback() throws UserDefinedException;

}
