package net.sf.vgrs.moneyTransfer.dao;

import net.sf.vgrs.moneyTransfer.domain.Client;
import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException;

import java.util.List;

public interface ClientDao {

    Client addClient(Client client) throws UserDefinedException;

    List<Client> getClients() throws UserDefinedException;

    Client getClient() throws UserDefinedException;

    boolean deleteClient(long clientId) throws UserDefinedException;
}
