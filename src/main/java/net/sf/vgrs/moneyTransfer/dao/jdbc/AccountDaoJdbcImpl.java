package net.sf.vgrs.moneyTransfer.dao.jdbc;

import net.sf.vgrs.moneyTransfer.dao.AbstractDao;
import net.sf.vgrs.moneyTransfer.dao.AccountDao;
import net.sf.vgrs.moneyTransfer.dao.ConnectionManager;
import net.sf.vgrs.moneyTransfer.domain.Account;
import net.sf.vgrs.moneyTransfer.domain.MoneyTransactionInfo;
import net.sf.vgrs.moneyTransfer.utlis.Constants;
import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AccountDaoJdbcImpl extends AbstractDao implements AccountDao {

    private static enum transactionType {block, in, out, cancel}

    private JDBCConnectionManager cm;

    @Inject
    public AccountDaoJdbcImpl(@Named(Constants.ANNOTATION_JDBC_CONNECTION_MAN)
                                          ConnectionManager connectionManager) {
        super(connectionManager);
        this.cm = (JDBCConnectionManager) connectionManager;
    }

    @Override
    public Long addAccount(Account account) throws UserDefinedException {
        try (PreparedStatement ps = cm.getConnection()
                             .prepareStatement(
                                     "insert into acct.accounts(client_id, currency)values (?,?)"
                                     ,Statement.RETURN_GENERATED_KEYS)){
            ps.setLong(1, account.getClientId());
            ps.setString(2, account.getCurrency().name());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if ( rs.next() ) {
                return rs.getLong(1);
            }
            throw new UserDefinedException(
                    UserDefinedException.Errors.ERROR_BUSINESS_LEVEL);
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new UserDefinedException(
                    UserDefinedException.Errors.ERROR_DB_CALL, ex);
        }
    }

    @Override
    public boolean updateAccountBalance(long accountId, BigDecimal amountToOperate) throws UserDefinedException {
        try {
            PreparedStatement ps = cm.getConnection()
                    .prepareStatement("update acct.accounts a set a.current_balance = a.current_balance + ? where acct_num = ?");
            ps.setBigDecimal(1, amountToOperate);
            ps.setLong(2, accountId);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new UserDefinedException(
                    UserDefinedException.Errors.ERROR_DB_CALL, ex);
        }
    }

    @Override
    public Account getAccountInfo(long accountNum) throws UserDefinedException {
        try {
            PreparedStatement ps = cm.getConnection()
                    .prepareStatement("select a.client_id, a.acct_num, a.current_balance, a.currency from acct.accounts a where a.acct_num = ?");
            ps.setLong(1, accountNum);
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                Account account = new Account();
                account.setClientId(rs.getLong(1));
                account.setAccountNumber(rs.getLong(2));
                account.setCurrentBalance(rs.getBigDecimal(3));
                account.setCurrency(Constants.Currencies.valueOf(rs.getString(4)));
                return account;
            } else {
                throw new UserDefinedException(
                        UserDefinedException.Errors.ERROR_BUSINESS_LEVEL, "Account not found");
            }
        } catch (SQLException ex) {
            throw new UserDefinedException(
                    UserDefinedException.Errors.ERROR_DB_CALL, ex);
        }
    }

    public List<Account> getAccounts(ConsumerWithException<PreparedStatement> setter) {
        List<Account> accounts = new ArrayList<>();
        try (PreparedStatement ps = cm.getConnection()
                .prepareStatement(
                        "select acct_num, client_id, currency, current_balance from acct.accounts where client_id = ? or ? is null;")){
            setter.accept(ps);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                Account account = new Account();
                account.setAccountNumber(rs.getLong(1));
                account.setClientId(rs.getLong(2));
                account.setCurrency(Constants.Currencies.valueOf(rs.getString(3)));
                account.setCurrentBalance(rs.getBigDecimal(2));
                accounts.add(account);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return accounts;
    }

    @Override
    public List<Account> getAccounts(long clientId) {
        return getAccounts(ps -> {
            ps.setLong(1, clientId);
            ps.setLong(2, clientId);
        });
    }

    @Override
    public List<Account> getAccounts() {
        return getAccounts(ps -> {
            ps.setNull(1, Types.BIGINT);
            ps.setNull(2, Types.BIGINT);
        });
    }

    @Override
    public MoneyTransactionInfo block(Long accountNumber, Long counterPartAccountNumber, BigDecimal amount)
            throws UserDefinedException {
        try {
            PreparedStatement ps = cm.getConnection()
                    .prepareStatement(
                            "insert into acct.money_transactions(acct_num, counterpart_acct_num, oper_date, amount, operation_type) values (?,?,?,?,?)",
                            Statement.RETURN_GENERATED_KEYS);

            ps.setLong(1, accountNumber);
            ps.setLong(2, counterPartAccountNumber);
            ps.setTimestamp(3, Timestamp.valueOf(LocalDateTime.now()));
            ps.setBigDecimal(4, amount);
            ps.setString(5, transactionType.block.name());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();

            if ( rs.next() ) {
                return new MoneyTransactionInfo(rs.getLong(1),
                        accountNumber,
                        counterPartAccountNumber,
                        amount);
            }
            throw new UserDefinedException(
                    UserDefinedException.Errors.ERROR_BUSINESS_LEVEL);
        } catch (SQLException ex) {
            throw new UserDefinedException(
                    UserDefinedException.Errors.ERROR_DB_CALL, ex);
        }
    }



    @Override
    public boolean credit(MoneyTransactionInfo info) throws UserDefinedException {
        try {
            PreparedStatement ps = cm.getConnection()
                    .prepareStatement(
                            "insert into acct.money_transactions(tr_num, acct_num, counterpart_acct_num, oper_date, amount, operation_type) values (?,?,?,?,?,?)");

            ps.setLong(1, info.getTransactionNumber());
            ps.setLong(2, info.getAccountNumber());
            ps.setLong(3, info.getCounterPartAccountNumber());
            ps.setTimestamp(4, Timestamp.valueOf(LocalDateTime.now()));
            ps.setBigDecimal(5, info.getAmount());
            ps.setString(6, transactionType.in.name());
            ps.executeUpdate();

            return true;

        } catch (SQLException ex) {
            throw new UserDefinedException(
                    UserDefinedException.Errors.ERROR_DB_CALL, ex);
        }
    }



    @Override
    public boolean debit(Long accountNum, Long transactionNumber) throws UserDefinedException {

        try {
            PreparedStatement ps = cm.getConnection()
                    .prepareStatement(
                            "update acct.money_transactions set operation_type = 'out' where acct_num = ? and tr_num = ?");

            ps.setLong(1, accountNum);
            ps.setLong(2, transactionNumber);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            throw new UserDefinedException(
                    UserDefinedException.Errors.ERROR_DB_CALL, ex);
        }
    }

    public interface ConsumerWithException<T>{
        void accept(T t) throws SQLException;
    }
}
