package net.sf.vgrs.moneyTransfer.web;

import net.sf.vgrs.moneyTransfer.web.controller.MoneyTransactionController;
import net.sf.vgrs.moneyTransfer.web.controller.ClientController;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/")
public class MoneyTransferApplication extends Application {

    private Set<Class<?>> classes = new HashSet<>();

    public MoneyTransferApplication() {
        classes.add(MoneyTransactionController.class);
        classes.add(ClientController.class);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }
}