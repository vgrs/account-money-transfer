package net.sf.vgrs.moneyTransfer.web.controller;

import net.sf.vgrs.moneyTransfer.dao.AccountDao;
import net.sf.vgrs.moneyTransfer.dao.ClientDao;
import net.sf.vgrs.moneyTransfer.dao.ConnectionManager;
import net.sf.vgrs.moneyTransfer.domain.Account;
import net.sf.vgrs.moneyTransfer.domain.Client;
import net.sf.vgrs.moneyTransfer.utlis.Constants;
import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static net.sf.vgrs.moneyTransfer.utlis.Constants.ANNOTATION_ACCOUNT_DAO_JDBC;
import static net.sf.vgrs.moneyTransfer.utlis.Constants.ANNOTATION_CLIENT_DAO_JDBC;
import static net.sf.vgrs.moneyTransfer.utlis.Constants.ANNOTATION_JDBC_CONNECTION_MAN;

@Path("/client-controller")
public class ClientController {

    //private static final Logger logger = LogManager.getLogger(ClientController.class);

    private ConnectionManager cm;

    private ClientDao clientDao;

    private AccountDao accountDao;

    public ClientController(){

    }

    @Inject
    public ClientController(@Named(ANNOTATION_JDBC_CONNECTION_MAN)
                                        ConnectionManager cm ,
                            @Named(ANNOTATION_CLIENT_DAO_JDBC)
                                           ClientDao clientDao ,
                            @Named(ANNOTATION_ACCOUNT_DAO_JDBC)
                                        AccountDao accountDao) {
        this.cm = cm;
        this.clientDao = clientDao;
        this.accountDao = accountDao;
    }

    @POST
    @Path("/add-client")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addClient(@HeaderParam(Constants.TAG_REQ_CODE)
                                          String reqCode ,
                              Client client) {
        try {
            System.out.println("method call addClient, reqCode is {}" + reqCode);
            cm.openConnection();
            Client addedClient = clientDao.addClient(client);
            System.out.println(addedClient);
            cm.commit();
            return Response.ok()
                    .header(Constants.TAG_REQ_CODE, reqCode)
                    .entity(addedClient).build();
        } catch (UserDefinedException e) {
            e.printStackTrace();
            return Response.serverError()
                    .status(Response.Status.BAD_REQUEST)
                    .header(Constants.TAG_STATUS, e.getError().format())
                    .header(Constants.TAG_REQ_CODE, reqCode)
                    .build();
        }
    }

    @GET
    @Path("/get-account-info")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccountInfo(@HeaderParam(Constants.TAG_REQ_CODE) String reqCode,
                               @QueryParam("account") Long account
    ) {
        try {
            cm.openConnection();
            Account accountInfo = accountDao.getAccountInfo(account);
            cm.closeConnection();
            return Response.ok()
                    .entity(accountInfo)
                    .header(Constants.TAG_REQ_CODE, reqCode)
                    .build();
        } catch (UserDefinedException e) {
            e.printStackTrace();
            return Response.serverError()
                    .status(Response.Status.BAD_REQUEST)
                    .header(Constants.TAG_STATUS, e.getError().format())
                    .header(Constants.TAG_REQ_CODE, reqCode)
                    .build();
        }
    }

    @GET
    @Path("/add-account")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addAccount(@HeaderParam(Constants.TAG_REQ_CODE) String reqCode,
                               Account account) {
        try {
            cm.openConnection();
            Long accountNum = accountDao.addAccount(account);
            account.setAccountNumber(accountNum);
            cm.commit();
            return Response.ok()
                    .entity(account)
                    .header(Constants.TAG_REQ_CODE, reqCode)
                    .build();
        } catch (UserDefinedException e) {
            e.printStackTrace();
            return Response.serverError()
                    .status(Response.Status.BAD_REQUEST)
                    .header(Constants.TAG_STATUS, e.getError().format())
                    .header(Constants.TAG_REQ_CODE, reqCode)
                    .build();
        }
    }
}
