package net.sf.vgrs.moneyTransfer.domain;

import java.math.BigDecimal;

public class MoneyTransactionInfo {

    private Long transactionNumber;
    private Long accountNumber;
    private Long counterPartAccountNumber;
    private BigDecimal amount;

    public MoneyTransactionInfo() {

    }

    public MoneyTransactionInfo(Long transactionNumber,
                                Long accountNumber,
                                Long counterPartAccountNumber,
                                BigDecimal amount) {
        this.transactionNumber = transactionNumber;
        this.accountNumber = accountNumber;
        this.counterPartAccountNumber = counterPartAccountNumber;
        this.amount = amount;
    }

    public Long getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(Long transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Long getCounterPartAccountNumber() {
        return counterPartAccountNumber;
    }

    public void setCounterPartAccountNumber(Long counterPartAccountNumber) {
        this.counterPartAccountNumber = counterPartAccountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
