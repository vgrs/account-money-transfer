package net.sf.vgrs.moneyTransfer;

import net.sf.vgrs.moneyTransfer.dao.AccountDao;
import net.sf.vgrs.moneyTransfer.dao.ClientDao;
import net.sf.vgrs.moneyTransfer.dao.ConnectionManager;
import net.sf.vgrs.moneyTransfer.dao.jdbc.*;
import net.sf.vgrs.moneyTransfer.domain.Account;
import net.sf.vgrs.moneyTransfer.domain.Client;
import net.sf.vgrs.moneyTransfer.domain.DebitDetails;
import net.sf.vgrs.moneyTransfer.domain.MoneyTransactionInfo;
import net.sf.vgrs.moneyTransfer.service.MoneyTransactionService;
import net.sf.vgrs.moneyTransfer.utlis.Constants;
import net.sf.vgrs.moneyTransfer.utlis.UserDefinedException;
import net.sf.vgrs.moneyTransfer.web.controller.ClientController;
import net.sf.vgrs.moneyTransfer.web.controller.MoneyTransactionController;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.*;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MoneyTransactionTest {

    private static final String bank1_url = "jdbc:h2:~/b1;INIT=runscript from 'classpath:db/h2/init.sql'";
    private static final String bank2_url = "jdbc:h2:~/b2;INIT=runscript from 'classpath:db/h2/init.sql'";

    private static DataSourceManager dataSourceManagerBank1;
    private static AccountDao accountDaoBank1;
    private static ClientDao clientDaoBank1;
    private static ConnectionManager connectionManagerBank1;
    private static MoneyTransactionService moneyTransactionServiceBank1;
    private static MoneyTransactionController moneyTransactionControllerBank1;
    private static ClientController clientControllerBank1;

    private static DataSourceManager dataSourceManagerBank2;
    private static AccountDao accountDaoBank2;
    private static ClientDao clientDaoBank2;
    private static ConnectionManager connectionManagerBank2;
    private static MoneyTransactionService moneyTransactionServiceBank2;
    private static MoneyTransactionController moneyTransactionControllerBank2;
    private static ClientController clientControllerBank2;

    private static Client client1;
    private static Client client2;

    @BeforeAll
    static void beforeAll() throws UserDefinedException {
        dataSourceManagerBank1 = new H2ContextDataSourceManager(bank1_url, "sa", "sa");
        connectionManagerBank1 = new JDBCConnectionManager(dataSourceManagerBank1);
        accountDaoBank1 = new AccountDaoJdbcImpl(connectionManagerBank1);
        clientDaoBank1 = new ClientDaoJdbcImpl(connectionManagerBank1);
        moneyTransactionServiceBank1 = new MoneyTransactionService(connectionManagerBank1, accountDaoBank1);
        moneyTransactionControllerBank1 = new MoneyTransactionController(
                connectionManagerBank1, accountDaoBank1, moneyTransactionServiceBank1);

        clientControllerBank1 = new ClientController(
                connectionManagerBank1, clientDaoBank1, accountDaoBank1);

        dataSourceManagerBank2 = new H2ContextDataSourceManager(bank2_url, "sa", "sa");
        connectionManagerBank2 = new JDBCConnectionManager(dataSourceManagerBank2);
        accountDaoBank2 = new AccountDaoJdbcImpl(connectionManagerBank2);
        clientDaoBank2 = new ClientDaoJdbcImpl(connectionManagerBank2);
        moneyTransactionServiceBank2 = new MoneyTransactionService(connectionManagerBank2, accountDaoBank2);
        moneyTransactionControllerBank2 = new MoneyTransactionController(
                connectionManagerBank2, accountDaoBank2, moneyTransactionServiceBank2);

        clientControllerBank2 = new ClientController(
                connectionManagerBank2, clientDaoBank2, accountDaoBank2);

        // Initialize bank users and accounts

        client1 = initClientForBank("Vugar Suleymanov", clientControllerBank1);
        client2 = initClientForBank("Arnold Schwarzenegger", clientControllerBank2);
    }

    private static Client initClientForBank(String clientFullName, ClientController clientController) {
        Client client = new Client();
        client.setFullName(clientFullName);
        Response response = clientController.addClient("", client);
        client = (Client) response.getEntity();
        System.out.printf("added client %s", client);
        Account account1 = (Account) clientController
                .addAccount("", new Account(client.getId(), Constants.Currencies.USD)).getEntity();
        List<Account> accounts = new ArrayList<>();
        accounts.add(account1);
        client.setAccounts(accounts);
        return client;
    }

    @AfterEach
    void tearDown() {

    }


    @Test
    @DisplayName("Test/simulate money transaction between 2 banks")
    public void testSuccessFullMoneyTransaction() throws InterruptedException {

        BigDecimal operationAmount = BigDecimal.valueOf(500);

        Account account1InfoBefore = (Account) clientControllerBank1.getAccountInfo(
                "", client1.getAccounts().get(0).getAccountNumber()).getEntity();

        Account account2InfoBefore = (Account) clientControllerBank2.getAccountInfo(
                "", client2.getAccounts().get(0).getAccountNumber()).getEntity();

        System.out.printf("Before transfer : account1 balance : %s, account2 balance :%s",
                account1InfoBefore.getCurrentBalance(), account2InfoBefore.getCurrentBalance());
        System.out.println();

        MoneyTransactionInfo blockInfo = new MoneyTransactionInfo();
        blockInfo.setAccountNumber(client1.getAccounts().get(0).getAccountNumber());
        blockInfo.setCounterPartAccountNumber(client2.getAccounts().get(0).getAccountNumber());
        blockInfo.setAmount(operationAmount);
        Response blockAmountResponse = moneyTransactionControllerBank1.blockAmount("", blockInfo);

        blockInfo = (MoneyTransactionInfo) blockAmountResponse.getEntity();
        assertEquals(HttpStatus.SC_OK, blockAmountResponse.getStatus());

        Thread.sleep(10000);

        MoneyTransactionInfo creditInfo = new MoneyTransactionInfo();

        creditInfo.setTransactionNumber(blockInfo.getTransactionNumber());
        creditInfo.setAccountNumber(client2.getAccounts().get(0).getAccountNumber());
        creditInfo.setCounterPartAccountNumber(client1.getAccounts().get(0).getAccountNumber());
        creditInfo.setAmount(operationAmount);
        Response creditAmountResponse = moneyTransactionControllerBank2.creditAmount("", creditInfo);

        assertEquals(HttpStatus.SC_OK, creditAmountResponse.getStatus());

        DebitDetails debitDetails = new DebitDetails();
        debitDetails.setAccountNumber(blockInfo.getAccountNumber());
        debitDetails.setTransactionNumber(blockInfo.getTransactionNumber());

        moneyTransactionControllerBank1.debitAmount("", debitDetails);

        Account account1InfoAfter = (Account) clientControllerBank1.getAccountInfo(
                "", client1.getAccounts().get(0).getAccountNumber()).getEntity();

        Account account2InfoAfter = (Account) clientControllerBank2.getAccountInfo(
                "", client2.getAccounts().get(0).getAccountNumber()).getEntity();


        System.out.printf("After transfer : account1 balance : %s, account2 balance :%s",
                account1InfoAfter.getCurrentBalance(), account2InfoAfter.getCurrentBalance());

        assertEquals(account1InfoAfter.getCurrentBalance(),
                account1InfoBefore.getCurrentBalance().subtract(operationAmount));

        assertEquals(account2InfoAfter.getCurrentBalance(),
                account2InfoBefore.getCurrentBalance().add(operationAmount));
    }

    @Test
    @DisplayName("Test/simulate money unsuccessful transaction")
    public void testUnSuccessFullMoneyTransaction() throws InterruptedException {

        BigDecimal operationAmount = BigDecimal.valueOf(1500);

        Account account1InfoBefore = (Account) clientControllerBank1.getAccountInfo(
                "", client1.getAccounts().get(0).getAccountNumber()).getEntity();


        MoneyTransactionInfo blockInfo = new MoneyTransactionInfo();
        blockInfo.setAccountNumber(client1.getAccounts().get(0).getAccountNumber());
        blockInfo.setCounterPartAccountNumber(client2.getAccounts().get(0).getAccountNumber());
        blockInfo.setAmount(operationAmount);

        Response blockAmountResponse =
                moneyTransactionControllerBank1.blockAmount("", blockInfo);

        assertEquals(HttpStatus.SC_BAD_REQUEST, blockAmountResponse.getStatus());

        assertTrue(blockAmountResponse.getHeaderString(Constants.TAG_STATUS)
                .contains(UserDefinedException.Errors.ERROR_BUSINESS_LEVEL.getCode()));

        Account account1InfoAfter = (Account) clientControllerBank1.getAccountInfo(
                "", client1.getAccounts().get(0).getAccountNumber()).getEntity();

        assertEquals(account1InfoBefore.getCurrentBalance(), account1InfoAfter.getCurrentBalance());

    }

}
